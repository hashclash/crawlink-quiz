import { Injectable } from '@angular/core';
import { CanActivate } from "@angular/router";
import { AuthService } from '../providers/auth.service';
import { fail } from 'assert';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router'
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router/src/router_state';
import { isNullOrUndefined } from 'util';
import * as firebase from 'firebase'
@Injectable()
export class AuthGuardService implements CanActivate {

  private auth: firebase.auth.Auth;

  constructor(private authService: AuthService,
    private router: Router) {
    this.auth = firebase.auth();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    let url: string = state.url;
    console.log('canActivate');
    return this.checkSignin(url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return this.canActivate(route, state);
  }


  checkSignin(url: string): Promise<boolean> {

    return new Promise((resolve, reject) => {
      this.auth.onAuthStateChanged((user) => {
        if (user) {
          resolve(true);
        } else {
          // console.log("Reject auth state!")
          resolve(false);
          this.router.navigate(['/login']);
        }
      });
    });
  }

  }
