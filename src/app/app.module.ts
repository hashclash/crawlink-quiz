import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, RouterOutlet } from '@angular/router';
import { MaterialModule } from './material/material.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { environment } from './../environments/environment';
import * as firebase from 'firebase'
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { InstructionComponent } from './instruction/instruction.component';
import { QuizComponent } from './quiz/quiz.component';
import { FinishComponent } from './finish/finish.component';
import { RoutingModule } from './routing/routing.module';
import { UserProviderService } from './providers/user-provider.service';
import { QuestionComponent } from './question/question.component'
import { QuestionService } from './providers/question.service';
import { LoginComponent } from './login/login/login.component'
import { AuthService } from './providers/auth.service'
import { AuthGuardService } from './guards/auth-guard.service'
import { DataProvider } from './providers/data-provider.service'
import { AppServiceProvider } from './providers/app.service'

firebase.initializeApp(environment.firebase)
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InstructionComponent,
    QuizComponent,
    FinishComponent,
    QuestionComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    RoutingModule,
    MaterialModule,

  ],
  providers: [ErrorStateMatcher
    , UserProviderService
    , QuestionService
    , AuthService
    , AuthGuardService
    , DataProvider,AppServiceProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
