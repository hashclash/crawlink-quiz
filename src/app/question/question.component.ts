import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Params } from '@angular/router/src/shared';
import { QuestionService } from '../providers/question.service';
import { Question } from '../models/Question';
import { DataProvider } from '../providers/data-provider.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  question_no: number;
  public question: Question;
  public questions: Array<Question>;
  t

  constructor(
    public questionService: QuestionService,
    private activatedRoute: ActivatedRoute,
    private dataProvider: DataProvider,
    private router: Router) {
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(params => {
        this.question_no = params['id'];
        this.questionService.setActiveQuestion(this.question_no);

      })

    this.questionService.getQuestions()
    .subscribe(res=>{
      this.questions = res;
    
    })
  }

  public goPrevious() {
    if (this.question_no > 1) {
      var prev = this.question_no - 1;
      this.question_no = prev;
      this.router.navigate(['quiz/question/' + prev], {})
      this.questionService
        .setActiveQuestion(prev);
    
    }
  }

  public goNext() {
    if (this.question_no < this.questions.length) {
      var next = ++this.question_no;
      this.question_no = next;
      this.router.navigate(['quiz/question/' + next], {})
      this.questionService
        .setActiveQuestion(next);
    }

  }

  public doSubmit() {

  }
}
