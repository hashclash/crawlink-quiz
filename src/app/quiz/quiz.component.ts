import { Component, OnInit } from '@angular/core';
import { Question } from '../models/Question'
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from '../providers/question.service';
import { ChangeDetectorRef } from '@angular/core';
import { DataProvider } from '../providers/data-provider.service';
import { AppServiceProvider } from '../providers/app.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {
  public questions: any;
  public question: string;
  public question_no: number;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private questionService: QuestionService,
    private dataProvider: DataProvider,
    private activatedRoute: ActivatedRoute,
    public appService:AppServiceProvider
  ) {
  }

  ngOnInit() {
    this.dataProvider.getAllQuestion()
      .then(array => {
        this.questionService.setQuestions(array);
      })

      this.questionService.getQuestions().subscribe(res=>{
        this.questions = res;
        this.appService.startTimer();

      })

    this.moveToQuestion(1);

    this.questionService.getActiveQuestion()
      .subscribe(q => {
        this.question_no = q;

      })

  }

  public moveToQuestion(i) {

    this.router.navigate(['quiz/question/' + i]);
    this.questionService
      .setActiveQuestion(i);
  }
  // ngAfterViewChecked() {
  //   console.log("after view changed");
  //   this.cdRef.detectChanges();
  // }
}
