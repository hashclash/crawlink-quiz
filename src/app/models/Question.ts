export class Question{
    id:string;
    question:string;
    op1:string;
    op2:string;
    op3:string;
    op4:string;
    ans:string;

    selected:string;
    constructor(){
        this.selected="";
    }
}