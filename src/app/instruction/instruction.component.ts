import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import{MyErrorStateMatcher} from '../MyErrorStateMatcher';
import { User } from '../models/User';
import { UserProviderService } from '../providers/user-provider.service';
import { log } from 'util';
import {Router} from '@angular/router';

@Component({
  selector: 'app-instruction',
  templateUrl: './instruction.component.html',
  styleUrls: ['./instruction.component.css']
})

export class InstructionComponent implements OnInit {

  user:User;

  constructor(private userProviderService:UserProviderService, private router:Router) { 
    this.user = new User();

  }

  ngOnInit() {
  }

  public doLogin(){
    if(!this.user.name){
      //show snackbar
      console.log("name-required!")
      return;
    }
    if(!this.user.email){
      //show snackbar
      console.log("email-required!")
      return;
    }
    //else login
    this.userProviderService.setUser(this.user);
    this.router.navigate(['/home'],{});
    console.log("User logged in successfully");
  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  nameFormControl = new FormControl('',[
    Validators.required
  ])

  matcher = new MyErrorStateMatcher();
 

}
