import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';
import 'firebase/auth'
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AuthService {
  public email: string;
  public authUser: any;
  public isLoggedIn:boolean;
  constructor() {

    firebase.auth()
      .onAuthStateChanged((user) => {
        if (user) {
          this.authUser = user;
          console.log("logged in:");
          console.dir(user)
          this.isLoggedIn = true;
        } else {
          console.log(user);
          this.isLoggedIn = false;
          this.authUser = user;
        }
      })

  }

  public signUp(email: string, password: string) {
    return new Promise((resolve,reject)=>{
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(data=>{
        resolve(data);
      })
      .catch(err=>{
        reject(err);
      })
    }) 
  }

  public login(email: string, password: string) {
    return new Promise((resolve,reject)=>{
      firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      .then(res=>{
          return firebase.auth().signInWithEmailAndPassword(email, password);
      })
      .then(data=>{
        this.isLoggedIn = true;
        resolve(data);
      })
      .catch(err=>{
        reject(err);
      })
    }) 
  }

  public logout() {

    return new Promise((resolve, reject) => {
      return firebase.auth()
        .signOut()
        .then(res => {
          this.isLoggedIn = false;
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });

  }

}
