import { Injectable } from '@angular/core';
import { User } from '../models/User'
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class UserProviderService {
      private subject = new Subject<any>();

  user: User;
  constructor() {
    this.user = new User;
  }

  public setUser(user) {
    this.user = user;
    this.subject.next(this.user);

  }

  public getUser():Observable<User> {
    return this.subject.asObservable();
  }
}
