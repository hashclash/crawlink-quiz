import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { Question } from '../models/Question';
@Injectable()
export class QuestionService {
  private subject = new Subject<any>();
  private questionSubject = new Subject<Array<Question>>();
  private questions: Array<Question>;
  private currentQuestion:any;
  constructor() {
    this.questions = [];
  }
  public setActiveQuestion(qno) {
    this.subject.next(qno);
  }

  public setQuestions(questions) {
    this.questionSubject.next(questions);
    this.questions = questions;
  }

  public getQuestions():Observable<Array<Question>>{
    return this.questionSubject.asObservable();
  }

  public getActiveQuestion(): Observable<number> {
    return this.subject.asObservable();
  }
}
