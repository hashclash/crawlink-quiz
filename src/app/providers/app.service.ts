import { Injectable } from '@angular/core'
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class AppServiceProvider {

    public timeSubject = new Subject<any>();
    public now: any;
    public difference: any;
    public countdown: any;
    public minutes: any;
    public seconds: any;
    public timeUp: boolean;
    public interval: any;
    constructor() {
        this.minutes = 30;
        this.seconds=0;
        this.countdown = new Date(+new Date() + 1.8e6).getTime();
        this.timeUp = false;
     
    }

  public startTimer(){
    this.interval = setInterval(() => {
        this.now = new Date().getTime();;
        this.difference = this.countdown - this.now;
        this.minutes = Math.floor((this.difference % (1000 * 60 * 60)) / (1000 * 60));
        this.seconds = Math.floor((this.difference % (1000 * 60)) / 1000);

        if (this.difference < 0) {
            this.timeUp = true;
            clearInterval(this.interval);
        }
    }, 1000);

  }

  public stopTimer(){
    clearInterval(this.interval);
  }



}