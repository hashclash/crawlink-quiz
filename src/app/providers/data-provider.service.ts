import { Injectable } from '@angular/core'
import * as firebase from 'firebase'
import 'firebase/firestore'
import { Question } from '../models/Question';
@Injectable()
export class DataProvider {

    constructor() {

    }
    public getAllQuestion() {
        let array =[];
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('crawlink')
                .doc('quiz')
                .collection('questions')
                .get()
                .then((snap) => {
                    snap.docs.forEach((doc) => {                     
                        array.push(doc.data());
                    })

                    resolve(array);
                })
                .catch((err) => {
                    reject(err);
                })
        })
    }
}