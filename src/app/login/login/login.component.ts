import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../providers/auth.service';
import { User } from 'firebase/app';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import * as firebase from 'firebase'
import 'firebase/auth'
import { MyErrorStateMatcher } from '../../MyErrorStateMatcher';
import { Router } from '@angular/router';
import { log } from 'util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;
  email: string;
  password: string;
  signing_in: boolean;
  constructor(private authService: AuthService, private router: Router) { }


  ngOnInit() {
    // this.signing_in = true;
    // firebase.auth()
    //   .onAuthStateChanged(res => {
    //     if (res) {
    //       this.signing_in = false;
    //       console.log("already logged");

    //       this.router.navigate(['/home'], {});
    //     }


    //   });

  }

  public signup() {

    if (!this.password) {
      //show snackbar
      console.log("name-required!")
      return;
    }
    if (!this.email) {
      //show snackbar
      console.log("email-required!")
      return;
    }
    if (!this.password) {
      //show snackbar
      console.log("name-required!")
      return;
    }
    this.signing_in = true;
    this.authService.signUp(this.email, this.password)
      .then(value => {
        console.log("User created!");
        this.login(this.email, this.password);
      })
      .catch(err => {
        console.log("something went wrong! " + err.code);
        console.log("message: " + err.message);
        console.log(err.code);
        if (err.code == "auth/email-already-in-use") {

          this.login(this.email, this.password);
        } else {
          this.signing_in = false;
        }
      })
  }

  public login(email, password) {
    this.authService.login(email, password)
      .then(res => {
        this.signing_in = false;
        //navigate to next route
        this.authService.isLoggedIn = true;
        this.router.navigate(['/home'], {});
        console.log("User logged in successfully");

      })
      .catch(err => {
        if (err.code == "auth/wrong-password") {
          this.signing_in = false;
        }
        console.log("something went wrong! " + err.code);
        console.log("message: " + err.message);
      })


  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  nameFormControl = new FormControl('', [
    Validators.required
  ])

  matcher = new MyErrorStateMatcher();
}
