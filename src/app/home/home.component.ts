import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { UserProviderService } from '../providers/user-provider.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
user:User;
  constructor(private userProviderService:UserProviderService) {
    this.user = new User;
   }

  ngOnInit() {
    this.userProviderService.getUser()
    .subscribe(user=>{
      this.user = user;
    })
  }

}
