import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { QuizComponent } from '../quiz/quiz.component';
import { QuestionComponent } from '../question/question.component';
import { LoginComponent } from '../login/login/login.component';
import { AuthGuardService } from '../guards/auth-guard.service'

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent, canActivate:[AuthGuardService]},
  { path: 'login', component: LoginComponent},
  {
    path: 'quiz',canActivateChild:[AuthGuardService],
    component: QuizComponent,
    children: [
      { path: 'question/:id', component: QuestionComponent }
    ]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }

];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    )
    // other imports here
  ],
  declarations: []
})
export class RoutingModule { }
