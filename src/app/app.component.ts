import { Component } from '@angular/core';
import { UserProviderService } from './providers/user-provider.service';
import { User } from './models/User';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from './providers/auth.service';
import { Observable } from '@firebase/util/dist/esm/src/subscribe';
import * as firebase from 'firebase';
import {Router} from '@angular/router';
import { AppServiceProvider } from './providers/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor( public authService:AuthService,
    public appService:AppServiceProvider,
     private router:Router,
    ) {
  }

  public logout(){

    this.authService
    .logout()
    .then(res=>{
      //navigate to login page
      this.appService.stopTimer();
        this.router.navigate(['/login']);
    })
  }
}
